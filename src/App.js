import { Route, Switch } from "react-router-dom";
import AllContacts from "./containers/AllContacts/AllContacts";
import Layout from "./components/UI/Layout/Layout";

const App = () => {
  return (
    <Layout>
      <Switch>
        <Route path="/" exact component={AllContacts} />
      </Switch>
    </Layout>
  );
};

export default App;
