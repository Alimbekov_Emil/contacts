import React from "react";
import { Link } from "react-router-dom";
import "./Layout.css";

const Layout = ({ children }) => {
  return (
    <>
      <header className="header">
        <Link to="/" className="logo">
          Contact book
        </Link>
      </header>
      <main className="container">{children}</main>
    </>
  );
};

export default Layout;
