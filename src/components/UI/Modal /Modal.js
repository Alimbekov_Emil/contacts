import React, { useState } from "react";
import { loadFromLocalStorage, saveToLocalStorage } from "../../../containers/localStorage";
import Backdrop from "../Backdrop/Backdrop";
import "./Modal.css";

const Modal = ({ id, name, username, email, phone, closed, show, setSortContacts }) => {
  const [state, setState] = useState({
    name,
    username,
    email,
    phone,
  });

  const inputChangeHandler = (e) => {
    const { value, name } = e.target;

    setState((prev) => ({ ...prev, [name]: value }));
  };

  const saveContact = (id) => {
    const localStorageContacts = loadFromLocalStorage();
    try {
      localStorageContacts[id].name = state.name;
      localStorageContacts[id].phone = state.phone;
      localStorageContacts[id].username = state.username;
      localStorageContacts[id].email = state.email;
      saveToLocalStorage(localStorageContacts);
      setSortContacts(localStorageContacts);
    } catch (e) {
      console.log(e);
    }
    closed();
  };

  return (
    <>
      <Backdrop show={show} onClick={closed} />
      <div
        className="Modal"
        style={{
          transform: show ? "translateX(0)" : "translateX(-1000vh)",
          opacity: show ? "1" : "0",
        }}
      >
        <b>Name</b>
        <input value={state.name} onChange={inputChangeHandler} name="name" className="field" />
        <b>User Name</b>
        <input value={state.username} onChange={inputChangeHandler} name="username" className="field" />
        <b>Email</b>
        <input value={state.email} onChange={inputChangeHandler} name="email" className="field" />
        <b>Phone</b>
        <input value={state.phone} onChange={inputChangeHandler} name="phone" className="field" />
        <div>
          <button onClick={() => saveContact(id)} type="button">
            Save
          </button>
          <button onClick={closed} type="button">
            Close
          </button>
        </div>
      </div>
    </>
  );
};

export default Modal;
