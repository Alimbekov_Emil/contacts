import axios from "axios";
import React, { useEffect, useState } from "react";
import Contact from "../Contact/Contact";
import Fuse from "fuse.js";
import "./AllContacts.css";
import SearchIcon from "@material-ui/icons/Search";
import { loadFromLocalStorage, saveToLocalStorage } from "../localStorage";

const options = { minMatchCharLength: 1, keys: ["name"] };

const AllContacts = () => {
  const [contacts, setContacts] = useState([]);
  const [sortContacts, setSortContacts] = useState([]);
  const [input, setInput] = useState("");

  useEffect(() => {
    const fetchData = async () => {
      const response = await axios.get("https://demo.sibers.com/users");
      const localStorageContacts = loadFromLocalStorage();
      if (!localStorageContacts) saveToLocalStorage(response.data);
      setContacts(localStorageContacts);
    };
    fetchData().catch(console.error);
  }, [contacts]);

  const contactsHandler = (array) => {
    if (array) {
      return array.map((contact) => (
        <Contact
          key={contact.id}
          id={contact.id}
          setSortContacts={setSortContacts}
          name={contact.name}
          avatar={contact.avatar}
          email={contact.email}
          phone={contact.phone}
          username={contact.username}
        />
      ));
    }
  };

  const onChange = (e) => {
    const fuse = new Fuse(sortContacts, options);
    const result = fuse.search(e.target.value).map((contact) => contact.item);
    setInput(e.target.value);
    if (input === "") {
      setSortContacts(contacts);
    } else {
      setSortContacts(result);
    }
  };

  return (
    <>
      <div className="search-block">
        <input className="field" value={input} onChange={onChange} placeholder="Search your contact" />
        <SearchIcon />
      </div>
      <div className="contacts-block">
        {input ? contactsHandler(sortContacts) : contactsHandler(contacts)}
      </div>
    </>
  );
};
export default AllContacts;
