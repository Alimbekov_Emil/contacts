import React, { useState } from "react";
import Modal from "../../components/UI/Modal /Modal";
import "./Contact.css";

const Contact = ({ id, name, username, email, phone, setSortContacts }) => {
  const [modal, setModal] = useState(false);

  return (
    <>
      <div className="card">
        <h3>{name}</h3>
        <p>User Name: {username}</p>
        <p>Email: {email}</p>
        <p>Phone: {phone}</p>
        {/* <img src={avatar} alt="avatar" /> */}
        {/*/image request denied/*/}
        <button type="button" onClick={() => setModal(true)}>
          edit
        </button>
      </div>
      <Modal
        id={id}
        show={modal}
        name={name}
        username={username}
        email={email}
        phone={phone}
        setSortContacts={setSortContacts}
        closed={() => setModal(false)}
      />
    </>
  );
};

export default Contact;
